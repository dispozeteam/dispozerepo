        <div class="innerbanner arb">
            <div class="innertitle">
            <h3>منتجات</h3>
            </div>
        </div>
  </div>
</div>
<div class="cntmain">
  <div class="container">
    <div class="productmain-dtls">
      <div class="row">
          <?php if(count($products>0)){
              foreach ($products as $product){
              ?>
           <div class="col-sm-3 col-sm-push-9">
                <div class="productdtlsleft">
                    <span>
                    <img src="<?=base_url().$product->image;?>"  class="img-responsive"  alt=""/>
                    </span>
                </div>
           </div>
           <div class="col-sm-9 col-sm-pull-3 arb">
              <div class="productdtlsright">
                <h4><?=$product->product;?></h4>
                <?php 
                $des=$product->descriptionArabi;
                $ptype=$product->product_typearabic;
                $ppaking=$product->packingarabi;
                $pcolor=$product->color_arabi;
                $pmetirials=$product->meterial_arabi;
                $pitem_size=$product->item_size;
                $pgsweight=$product->gsweight;
                $pcapacity=$product->capacity;
                $pthickness=$product->thickness;
                $pimage=$product->image;
                ?>
                <?php if($des!="") {?>
                <div class="row">
                  <div class="col-sm-12"><?= $des?></div>
                  
                </div>
                <?php }?>

                <div class="containerforfutures">
                <?php if($ptype!="") {?>
                <div class="row" >
                  <div class="col-sm-2 arb">:نوع المنتج</div>
                  <div class="col-sm-10 arb"><?= $ptype?></div>
                </div>
                <?php }?>

                <?php if($ppaking!="") {?>
                <div class="row">
                  <div class="col-sm-2">:التعبئة</div>
                  <div class="col-sm-10"><?=$ppaking?></div>
                </div>
                <?php }?>

                <?php if($pcolor!="") {?>
                <div class="row">
                  <div class="col-sm-2">:اللون </div>
                  <div class="col-sm-10"><?=$pcolor?></div>
                </div>
                <?php }?>

                <?php if($pmetirials!="") {?>
                <div class="row">
                  <div class="col-sm-2">:المواد</div>
                  <div class="col-sm-10"><?=$pmetirials?></div>
                </div>
                <?php }?>

                 <?php if($pitem_size!="") {?>
                <div class="row">
                  <div class="col-sm-2">:حجم البند</div>
                  <div class="col-sm-10"><?=$pitem_size?></div>
                </div>
                <?php }?>

                <?php if($pgsweight!="") {?>
                <div class="row">
                  <div class="col-sm-2">:الوزن الإجمالي</div>
                  <div class="col-sm-10"><?=$pgsweight?></div>
                </div>
                <?php }?>

                 <?php if($pcapacity!="") {?>
                <div class="row">
                  <div class="col-sm-2">:سعة</div>
                  <div class="col-sm-10"><?=$pcapacity?></div>
                </div>
                <?php }?>

                 <?php if($pthickness!="") {?>
                <div class="row">
                  <div class="col-sm-2">:سماكة</div>
                  <div class="col-sm-10"><?=$pthickness?></div>
                </div>
                <?php }?>
                </div>
           </div>
          <?php } } ?>
      </div>
    </div>
  </div>
</div>
