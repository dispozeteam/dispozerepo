<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>مرحبا بكم في ديسبوز</title>
<link rel="shortcut icon" href="<?=base_url();?>img/fav.png" type="image/x-icon">
<link rel="icon" href="<?=base_url();?>img/fav.png" type="image/x-icon">

<!-- Bootstrap -->
<link href="<?=base_url();?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url();?>css/style.css" rel="stylesheet">
<link href="<?=base_url();?>css/inner.css" rel="stylesheet">
<link href="<?=base_url();?>https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Taviraj" rel="stylesheet">
<link href="<?=base_url();?>css/owl.carousel.css" rel="stylesheet" />
<link rel="stylesheet" href="<?=base_url();?>css/animate.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div id="headermain">
  <div class="container clearfix">
    <div class="headerleft"> <a href="<?=site_url();?>/fronthomeArb" class="logo"></a> </div>
    <div class="headerright clearfix">
      <div class="lng"> <a href="<?=site_url();?>/fronthome">Eng</a> | <a href="<?=site_url();?>/fronthomeArb">Arb</a> </div>
      <div class="headercontact">
        <div class="phone">+91 568941235</div>
        <div class="email"><a href="mailto:mail@gmail.com">mail@gmail.com</a></div>
      </div>
      <div class="headersocial">
        <div class="social"> <a href="#" target="_blank">
          <div class="fb"></div>
          </a> <a href="#">
          <div class="gplus"></div>
          </a><a href="#">
          <div class="twt"></div>
          </a> </div>
      </div>
    </div>
  </div>
</div>
<div class="container">

    <!-- for seession message -->
    <?php
    if($this->session->flashdata('flash')) { ?>
        <div style="width: 80%; margin: 0 auto;margin-bottom: 5px;" class="alert alert-success status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
            <?= $this->session->flashdata('flash')['message']; ?>
        </div>
    <?php } ?>
    <!-- Media dialog type1-->
</div>
