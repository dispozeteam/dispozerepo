          
<div class="innerbanner arb">
    <div class="innertitle">
        <h3>حول</h3>
    </div>
</div>
          
          
  </div>
</div>
<div class="cntmain">
  <div class="container">
    <div class=" whitearea aboutpage">
     
      <div class="row abouttop">
       <div class="col-sm-4 col-sm-push-8">
        <div class="aboutimg">
          <img src="<?=base_url();?>img/about.jpg"  class="img-responsive" alt=""/> </div>
       </div>
       <div class="col-sm-8 col-sm-pull-4">
        <div class="aboutright arb">
         <p>وقد ساعدت خمس سنوات من الخبرة في هذه الصناعة ومع مجموعة واسعة من حلول التعبئة والتغليف عالية الجودة والمستهلكات التي تلبي احتياجات الفنادق والمطاعم والمستشفيات وشركات الطيران وغيرها الكثير إنشاء صحارى للتجارة ذ.م.م باعتبارها واحدة من الموردين الأكثر بروزا في دولة الإمارات العربية المتحدة. مع خدمة العملاء وفريق المبيعات مخصص لعملائنا يمكن ضمان أعلى معايير الخدمة. ويضمن فريق تسليم عالي الكفاءة مع مخازن مجهزة بشكل جيد التسليم في الوقت المحدد في جميع أنحاء الإمارات العربية المتحدة.
             <br>
             <br>
With the المنتجات المبتكرة من أكثر من 50 البنود من رقائق الألومنيوم، حاويات، الأغطية البلاستيكية، أكياس، يتوهم نظارات، الشرب القش، ورقة كوب إلى المنتجات المخصصة ل مختلف قطاعات السوق في جميع أنحاء العالم. تخلص هو مصنع وموزع متكامل من مجموعة المتاح من البلاستيك والورق والألومنيوم. الذاتي-- مدفوعة، ونحن نقدم منتجات ذات جودة استثنائية التي تجعلنا تصنيع من الطراز العالمي لدينا القدرة على ملء أوامر واسعة النطاق للشركات الكبيرة وراسخة .. كل ما تحتاجه، نود أن مساعدتك.
</p>
        
        </div>
       </div>
      
      </div>
     
     

    </div>
    <div class="aboutbottom">
     <div class="vision clearfix">
      <div class="visionleft"><div class="misionimg"><img src="<?=base_url();?>img/vision.png"  class="img-responsive"  alt=""/></div></div>
      <div class="visionright">
          <div>
              <h4>رؤية</h4>
              <p>ضمان وتمكين وتحفيز جميع الموظفين لتعزيز ثقافة الابتكار والتحسين المستمر داخل المنظمة لإنشاء منظمة عالمية. </p>
          </div>
      </div> 
      
     </div>
     <div class="mission clearfix">
     <div class="missionright">
       <div class="missionimg"><img src="<?=base_url();?>img/mission.png"  class="img-responsive"  alt=""/></div>
      </div>
      <div class="missionleft">
       <h4>مهمة</h4>
       <p> لتفريق منتجاتنا في سوق مزدحمة، في حين تقدم الاستفادة من حزمة فعالة من حيث التكلفة وسهلة لتخزين. </p>
      </div>
      
     </div>
    
    </div>
    
    
  </div>
  
</div>