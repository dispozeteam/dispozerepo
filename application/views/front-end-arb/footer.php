<div class="footermain">
  <div class="container">
    <div class=" row">
      <div class="col-sm-4 footerleft col-sm-push-8 arb"> <a href="<?=site_url();?>/fronthomeArb" class="footerlogo"></a>
        <div class="footersocial">
        <h5> تابعنا</h5>
        <div class="social"> <a href="#" target="_blank">
          <div class="fb"></div>
          </a> <a href="#">
          <div class="gplus"></div>
          </a><a href="#" style="margin-right: 0px;">
          <div class="twt"></div>
          </a> </div>
      </div>
      </div>
      
      
       <div class="col-sm-4 arb">
        <h5>اتصل</h5>
        <p>
        Lorem Ipsum is simply  <br/>
and typesetting industry. <br/>
Lorem Ipsum has been <br/>
Mob:+91 568941235<br/>
Email:<a href="mailto:info@dispozeuae.com">info@dispozeuae.com</a>
        
        </p>
       
       </div>
        <div class="col-sm-4 col-sm-pull-8 arb">
        
         <div class="footerlink">
         <h5>حلقة الوصل</h5>
         <ul>
          <li><a href="<?=site_url();?>/fronthomeArb">الصفحة الرئيسية</a></li>
          <li><a href="<?=site_url();?>/aboutArb">معلومات عنا</a></li>
          <li><a href="<?=site_url();?>/productsArb">منتجات</a></li>
          <li><a href="<?=site_url();?>/news-eventArb">أخبار &amp; أمب؛ الأحداث</a></li>
          <li><a href="<?=site_url();?>/careerArb">مهنة</a></li>
         <li><a href="<?=site_url();?>/contactArb">اتصل بنا</a></li>
         </ul>
         
         <a href="#" class="saharalogo"></a>
         </div>
        
        </div>
      
      
    </div>
  </div>
</div>

<div class="copyright">Copyright <?php echo date('Y');?> · All rights reserved</div>                                                                                                                                    


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?=base_url();?>js/bootstrap.min.js"></script>
<script src="<?=base_url();?>js/owl.carousel.min.js"></script>
 <script src="<?= base_url(); ?>js/product.js"></script>

<script>
        $('#newsslide').owlCarousel({
            loop: true
            , margin: 10
            , responsiveClass: true
            , navContainer: '#nav-controls'
            , responsive: {
                0: {
                    items: 1
                    , nav: true
                    , autoplay: true
                    , autoplayTimeout: 6000
                }

            }
        })
    </script> 
    <script type="text/javascript">

    $(window).load(function() {
      
    var $container = $('.animate-grid .gallary-thumbs');
    $container.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }

    });
    $('.animate-grid .categories a').click(function() {
      
        $('.animate-grid .categories .active').removeClass('active');
        $(this).addClass('active');
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
        return false;
    });

});
  </script>
    <script>
        $('#testimonialslide').owlCarousel({
            loop: true
            , margin: 10
            , responsiveClass: true
			, dots:true,
			animateOut: 'fadeOut'
            , responsive: {
                0: {
                    items: 1
                    , autoplay: true
                    , autoplayTimeout: 4000,
					animateOut: 'fadeOut'
                }

            }
        })
    </script>

	

<script src="<?=base_url();?>js/vgallery.js"></script>
<script>
$(document).ready(function() {
    var vg = new vGallery({
        gallery: '#banner',
		pause: false,
        images: [
            "<?=base_url();?>img/banner1.jpg",
            "<?=base_url();?>img/banner2.jpg",
            "<?=base_url();?>img/banner3.jpg",
          
        ],
        indicators: {
            element: '#indicators',
            round: false,
            opacity:0.5,
        },
       text: {
            element: '#text',
            items: [
                
			'<div class="bannertext"><h3>Lorem Ipsum is simply </h3><p>Lorem Ipsum is simply dummy text of the printing and typesetting</p></div>',
                '<div class="bannertext"><h3>Lorem Ipsum is simply </h3><p>Lorem Ipsum is simply dummy text of the printing and typesetting</p></div>',
                '<div class="bannertext"><h3>Lorem Ipsum is simply </h3><p>Lorem Ipsum is simply dummy text of the printing and typesetting</p></div>'

                
            ],
        }
       
    });
    vg.start();
});
</script>
</body>
</html>