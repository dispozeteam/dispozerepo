
    <div class="innerbanner arb">

        <div class="innertitle">
            <h3>مهنة</h3>
        </div>
    </div>


    </div>
    </div>
    <div class="cntmain">
        <div class="container">
            <div class=" whitearea career">

                <div class="careertop">
                    <div class="row">
                        <div class="col-sm-3 col-sm-push-9">
                            <div class="careerimg arb"><img src="<?=base_url()?>img/career.jpg"  class="img-responsive" alt=""/></div>
                        </div>
                        <div class="col-sm-9 col-sm-pull-3">
                            <div class="careertopright arb">
                                <p> أنت على وشك تقديم بيانات شخصية كجزء من تطبيق الوظيفة الشاغرة. ديسبوز قيم خصوصيتك واتخذت الخطوات اللازمة لتلبية متطلبات خصوصية البيانات. سيتم تخزين التطبيق الخاص بك وجميع المعلومات ذات الصلة التي تقدمها لك مركزيا على نظامنا والبيانات الخاصة بك وسوف تكون متاحة فقط لموظفي ديسبوز لغرض التوظيف فقط.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="careerbottom ">
                    <div class="arb"><h4>المناصب المفتوحة</h4>
                        <ul>
                            <li>
                                <span> أست. مدير - التسويق:</span> صناعة النص الدموي القياسية من أي وقت مضى منذ 1500s، عندما استغرقت طابعة غير معروفة المطبخ من نوع وسارعت لجعل كتاب عينة نوع. وقد نجا خمسة قرون فحسب،
                            </li>
                        </ul>
                        <p>الرجاء إرسال سيرتك الذاتية التفصيلية إلى: <a href="mailto:career@dispozeuae.com">career@dispozeuae.com</a></p>
                    </div>
                </div>


                <div class="careerforms">
                    <div class="arb" > <h4>قدم الآن</h4></div>


                    <form action="<?=site_url();?>/career_mail" class="form2" method="post" enctype="multipart/form-data">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name*" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail*" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="contact-no" name="contact-no" placeholder="Contact No*" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="post" name="post" placeholder="Applied for the Post of*" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <textarea class="form-control2" rows="4" name="profile" placeholder="A brief about your profile :"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="file" name="file" id="exampleInputFile" class="inputfl">
                                </div>
                            </div>
                            <div class="col-sm-12">
                 <span class="submit">
              <input class="btn" type="submit" value="Submit">
              </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>