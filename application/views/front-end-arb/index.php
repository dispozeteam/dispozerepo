<div id="bannermain">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 col-sm-push-8 ">
        <div class="navimain">
          <div class="navigationarea hidden-xs arb"> <span class="menuicon"></span>
            <ul class="clearfix">
              <li class="activepage"><a href="<?=site_url();?>/fronthomeArb">الصفحة الرئيسية</a></li>
              <li><a href="<?=site_url();?>aboutArb">معلومات عنا</a></li>
              <li><a href="<?=site_url();?>productsArb">منتجات</a></li>
              <li><a href="<?=site_url();?>news-eventArb">أخبار وأحداث</a></li>
              <li><a href="<?=site_url();?>careerArb">مهنة</a></li>
              <li><a href="<?=site_url();?>contactArb">اتصل بنا</a></li>
              <a href="<?=base_url().CATALOGUE?>" class="cataloguebtn"><span></span>فهرس</a>
            </ul>
          </div>
          <nav class="navbar visible-xs" role="navigation"> 
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a href="<?=base_url();?>index.php" class="logo"></a></div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav navbar-right arb">
                <li class="activepage"><a href="<?=site_url();?>/fronthomeArb">الصفحة الرئيسية</a></li>
                <li><a href="<?=site_url();?>aboutArb">معلومات عنا</a></li>
                 <li><a href="<?=site_url();?>productsArb">منتجات</a></li>
              <li><a href="<?=site_url();?>news-eventArb">أخبار وأحداث</a></li>
              <li><a href="<?=site_url();?>careerArb">مهنة</a></li>
              <li><a href="<?=site_url();?>contactArb">اتصل بنا</a></li>
              <li><a href="<?=base_url().CATALOGUE?>">فهرس</a></li>
              </ul>
            </div>
            <!-- /.navbar-collapse --> 
          </nav>
        </div>
      </div>
      <div class="col-sm-8 col-sm-pull-4">
        <div id="banner">
          <div class="bannermain">
            <div id="indicators_wrapper">
              <div id="indicators"></div>
            </div>
            <div id="text" class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="cntmain">
  <div class="container">
    <div class="welcomearea">
      <div class="row">
        <div class="col-sm-4 col-sm-push-8">
          <div class="welcomelogo"><img src="<?=base_url();?>img/welcomeimg.jpg" class="img-responsive" alt=""/></div>
        </div>
        <div class="col-sm-8 arb col-sm-pull-4">
          <h1>مرحبا بك في <span>ديسبوز</span></h1>
          <p>وقد ساعدت خمس سنوات من الخبرة في هذه الصناعة ومع مجموعة واسعة من حلول التعبئة والتغليف عالية الجودة والمستهلكات التي تلبي احتياجات الفنادق والمطاعم والمستشفيات وشركات الطيران وغيرها الكثير إنشاء صحارى للتجارة ذ.م.م باعتبارها واحدة من الموردين الأكثر بروزا في دولة الإمارات العربية المتحدة. مع خدمة العملاء وفريق المبيعات مخصص لعملائنا يمكن ضمان أعلى معايير الخدمة. ويضمن فريق تسليم عالي الكفاءة مع مخازن مجهزة بشكل جيد التسليم في الوقت المحدد في جميع أنحاء الإمارات العربية المتحدة.</p>
          <a href="<?=site_url();?>/aboutArb" class="more">اقرأ أكثر</a> </div>
      </div>
    </div>
    <div class="newarrivals-area">
<div class="arb_new">
      <h3>الوافدين الجدد </h3>
</div>
      <div class="newarrivals">
        <ul class="clearfix">
            <?php if(count($products>0)){
                foreach ($products as $product){?>
          <li>
            <div class="imgbox"><img src="<?=base_url();?><?=$product['image'];?>" class="img-responsive" alt=""/></div>
          </li>
            <?php } }?>
          <li> <a href="<?=site_url();?>/productsArb"" class="morebtn"> <span> أكثر من <br/>
             منتجات</span> </a> </li>
        </ul>
      </div>
    </div>
    
    <div class="homebottom clearfix">
     <div class="col-sm-5">
      <div class="newshome arb">
       <h4>أخبار وأحداث</h4>
          <div class="newsslide clearfix">
                    <div id="nav-controls"></div>
                    <div class="owl-carousel" id="newsslide">
                        <?php if(count($news >0)){
                        foreach ($news as $newNews){?>
                        <div class="item">
                          <div class="newsdate">
                          <span><?=$newNews['day'];?></span><?=$newNews['month']." ".$newNews['year'];?>
                          </div>
                          <p><?= $newNews['titleArabi'];?></p>
                        </div>
                        <?php }} ?>
                    </div>
          </div>
      </div> 
     </div>
     <div class="col-sm-7">
      <div class="testimonialshome arb">
       <h4>الشهادات - التوصيات </h4>
       
       <div class="testimonialslide clearfix">
                   
                    <div class="owl-carousel" id="testimonialslide">
                        <div class="item">
                         <div class="testitext">  
                          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,  </p>
                          <div class="testiname"><span>Ram Gopal |</span> CEO ABC Group</div>
                          
                          </div>
                        </div>
                        <div class="item">
                          <div class="testitext">  
                          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took ... </p>
                          <div class="testiname"><span>Dinesh Kumar |</span> MD BBD Group</div>
                          </div>
                          
                        </div>
                        <div class="item">
                          <div class="testitext">  
                          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took ... </p>
                          <div class="testiname"><span>Shameer p h|</span> Manager Team lmc</div>
                          </div>
                          
                        </div>
                    </div>
                </div>
       
       
      </div>
     </div>
    </div>
    
    
  </div>
</div>