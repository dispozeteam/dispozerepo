<div id="bannermain" class="bannermaininner">
    <div class="container">

        <nav class="navbar innernavi" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a href="<?=site_url();?>" class="logo"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?=site_url();?>/fronthomeArb">الصفحة الرئيسية</a></li>
                    <li <?= $this->uri->segment(1)=='aboutArb'?'class="activepage"':'';?>><a href="<?=site_url();?>aboutArb">حول</a></li>
                    <li <?= $this->uri->segment(1)=='productsArb'?'class="activepage"':'';?>><a href="<?=site_url();?>productsArb">منتجات</a></li>

                    <li <?= $this->uri->segment(1)=='news-eventArb'?'class="activepage"':'';?>><a href="<?=site_url();?>news-eventArb">أخبار وأحداث</a></li>
                    <li <?= $this->uri->segment(1)=='careerArb'?'class="activepage"':'';?>><a href="<?=site_url();?>careerArb">مهنة</a></li>
                    <li <?= $this->uri->segment(1)=='contactArb'?'class="activepage"':'';?>><a href="<?=site_url();?>contactArb">اتصل بنا</a></li>
                    <li <?= $this->uri->segment(1)=='catalogue'?'class="activepage"':'';?>><a href="<?=base_url().CATALOGUE?>">فهرس</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>