
        <div class="innerbanner">
            <div class="innertitle">
            <h3>Contact Us</h3>
            </div>
        </div>  

  </div>
</div>
<div class="cntmain">
  <div class="container">
    <div class=" whitearea contactpage">
     <div class="row cnttop">
      <div class="col-md-4">
       <p>
     We’d love to hear from you. Whether you want to learn more about our services, chat about your project or just to say hello Let’s connect!
     </p>
      
      </div>
      <div class="col-md-4">
       <div class="cntctr">
       <div class="phone">+91 568941235</div> <br/>
       <div class="mail"><a href="mailto:mail@gmail.com"> mail@gmail.com</a></div>
       </div>
      </div>
      <div class="col-md-4">
      <div class="contactsocial">
        <div class="social"> <a href="#" target="_blank">
          <div class="fb"></div>
          </a> <a href="#">
          <div class="gplus"></div>
          </a><a href="#">
          <div class="twt"></div>
          </a>
        </div>
      </div>
      </div>
     
     </div>
     
     
     <div class="contactforms">
          <form action="<?=site_url();?>/contact_mail" class="form" method="post">
              <div class="row">
                  <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name*" required>
                      </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                          <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail*" required>
                      </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                          <input type="text" class="form-control" id="contact-no" name="contact-no" placeholder="Contact No" >
                      </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                         <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" >
                      </div>
                  </div>
                  <div class="col-sm-12">
                      <div class="form-group">
                          <textarea class="form-control2" rows="4" placeholder="Message*" name="message" id="message" required></textarea>
                      </div>
                  </div>
                  <div class="col-sm-12">
                      <span class="submit">
                        <input class="btn" type="submit" value="Submit">
                      </span>
                  </div>
              </div>
          </form>
        </div>

    </div>
    
  </div>
  
</div>

<div id="map">
        <div class="Flexible-container">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d36166.48938808983!2d75.7772436678195!3d11.266752317754467!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba65938563d4747%3A0x32150641ca32ecab!2sKozhikode%2C+Kerala!5e0!3m2!1sen!2sin!4v1491288684658" width="800" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
