<div id="bannermain" class="bannermaininner">
    <div class="container">

        <nav class="navbar innernavi" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a href="<?=site_url();?>" class="logo"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?=site_url();?>">Home</a></li>
                    <li <?= $this->uri->segment(1)=='about'?'class="activepage"':'';?>><a href="<?=site_url();?>about">About</a></li>
                    <li <?= $this->uri->segment(1)=='products'?'class="activepage"':'';?>><a href="<?=site_url();?>products">PRODUCTS</a></li>

                    <li <?= $this->uri->segment(1)=='news-event'?'class="activepage"':'';?>><a href="<?=site_url();?>news-event">News and Events</a></li>
                    <li <?= $this->uri->segment(1)=='career'?'class="activepage"':'';?>><a href="<?=site_url();?>career">Career</a></li>
                    <li <?= $this->uri->segment(1)=='contact'?'class="activepage"':'';?>><a href="<?=site_url();?>contact">Contact Us</a></li>
                    <li <?= $this->uri->segment(1)=='catalogue'?'class="activepage"':'';?>><a href="<?=base_url().CATALOGUE ?>">CATALOGUE</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>