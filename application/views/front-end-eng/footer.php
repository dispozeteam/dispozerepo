<div class="footermain">
  <div class="container">
    <div class=" row">
      <div class="col-sm-4 footerleft"> <a href="<?=site_url();?>" class="footerlogo"></a>
        <div class="footersocial">
        <h5> Follow Us</h5>
        <div class="social"> <a href="#" target="_blank">
          <div class="fb"></div>
          </a> <a href="#">
          <div class="gplus"></div>
          </a><a href="#">
          <div class="twt"></div>
          </a> </div>
      </div>
      </div>
      
      
       <div class="col-sm-4">
        <h5>Contact</h5>
        <p>
        Lorem Ipsum is simply  <br/>
and typesetting industry. <br/>
Lorem Ipsum has been <br/>
Mob:+91 568941235<br/>
Email:<a href="mailto:info@dispozeuae.com">info@dispozeuae.com</a>
        
        </p>
       
       </div>
        <div class="col-sm-4">
        
         <div class="footerlink">
         <h5>Link</h5>
         <ul>
          <li><a href="<?=site_url();?>">Home</a></li>
          <li><a href="<?=site_url();?>/about">About Us</a></li>
          <li><a href="<?=site_url();?>/products">Products</a></li>
          <li><a href="<?=site_url();?>/news-event">News &amp; Events</a></li>
          <li><a href="<?=site_url();?>/career">Career</a></li>
         <li><a href="<?=site_url();?>/contact">Contact Us</a></li>
         </ul>
         
         <a href="#" class="saharalogo"></a>
         </div>
        
        </div>
      
      
    </div>
  </div>
</div>

<div class="copyright">Copyright <?php echo date('Y');?> · All rights reserved</div>                                                                                                                                    


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?=base_url();?>js/bootstrap.min.js"></script>

<script src="<?=base_url();?>js/owl.carousel.min.js"></script>
 <script src="<?= base_url(); ?>js/product.js"></script>
      <script>
        
            $(document).ready(function() {

                $('.thumbnail').click(function(){
                      $('.modal-body').empty();
                    var title = $(this).parent('a').attr("title");
                    $('.modal-title').html(title);
                    $($(this).parents('div').html()).appendTo('.modal-body');
                    $('#myModal').modal({show:true});
                     });
                });
        </script>
<script type="text/javascript">

    $(window).load(function() {
      
    var $container = $('.animate-grid .gallary-thumbs');
    $container.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }

    });
    $('.animate-grid .categories a').click(function() {
      
        $('.animate-grid .categories .active').removeClass('active');
        $(this).addClass('active');
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
        return false;
    });

});
  </script>

<script>
        $('#newsslide').owlCarousel({
            loop: true
            , margin: 10
            , responsiveClass: true
            , navContainer: '#nav-controls'
            , responsive: {
                0: {
                    items: 1
                    , nav: true
                    , autoplay: true
                    , autoplayTimeout: 6000
                }

            }
        })
    </script> 
    <script>
        $('#testimonialslide').owlCarousel({
            loop: true
            , margin: 10
            , responsiveClass: true
			, dots:true,
			animateOut: 'fadeOut'
            , responsive: {
                0: {
                    items: 1
                    , autoplay: true
                    , autoplayTimeout: 4000,
					animateOut: 'fadeOut'
                }

            }
        })
    </script>

	

<script src="<?=base_url();?>js/vgallery.js"></script>
<script>
$(document).ready(function() {
    var vg = new vGallery({
        gallery: '#banner',
		pause: false,
        images: [
            "<?=base_url();?>img/banner1.jpg",
            "<?=base_url();?>img/banner2.jpg",
            "<?=base_url();?>img/banner3.jpg",
          
        ],
        indicators: {
            element: '#indicators',
            round: false,
            opacity:0.5,
        },
       text: {
            element: '#text',
            items: [
                
			'<div class="bannertext"><h3>Lorem Ipsum is simply </h3><p>Lorem Ipsum is simply dummy text of the printing and typesetting</p></div>',
                '<div class="bannertext"><h3>Lorem Ipsum is simply </h3><p>Lorem Ipsum is simply dummy text of the printing and typesetting</p></div>',
                '<div class="bannertext"><h3>Lorem Ipsum is simply </h3><p>Lorem Ipsum is simply dummy text of the printing and typesetting</p></div>'

                
            ],
        }
       
    });
    vg.start();
});
</script>


</body>
</html>