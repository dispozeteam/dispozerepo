
        <div class="innerbanner">
        
    <div class="innertitle">
    <h3>Products</h3>
   </div>
        </div>  
          
          
  </div>
</div>

<section>
    <div class="gallary animate-grid">
        <div class="container">
      <div class="row">
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="categories">
                                <ul>
                                    <li>
                                        <ol>

                                            <li><a href="#" data-filter="*" class="active">All</a></li>
                                            <?php  foreach ($category as $prd_cat){?>
                                            <li><a href="#" data-filter=".<?= $prd_cat->cat_english; ?>"><?= $prd_cat->cat_english; ?></a></li>
                                            <?php } ?>
                                        </ol>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            <div class="col-sm-9 overlap_prd">
          <div class="row gallary-thumbs">

             <!-- product start -->
             
             <?php foreach ($all_prd as $prd){  
              $cat=$prd['category1'];


              ?>      
                        <div class="col-md-4 col-sm-6 <?php echo $cat; ?>">
                            <div class="gallary-item pmd-z-depth">
                                <a href="<?=site_url()."products-dtls/".$prd['id']; ?>">
                                <div class="hover-bg">
              <img src="<?=base_url();?><?= $prd['prd_img'];?>" class="img-responsive" alt="...">

                                        <div class="hover-text">
                                            <h4><?= $prd['prd_name']; ?></h4>                                       
                                            <!-- <i class="fa fa-plus"></i> -->
                                        </div>
                                </div>
                              </a>
                            </div>
                        </div>
             <?php } ?>
        <!-- end product -->

                </div>
              
          </div>

      </div>
        </div>
  </div>
</section>



