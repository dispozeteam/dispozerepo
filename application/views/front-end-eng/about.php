
          
<div class="innerbanner">
    <div class="innertitle">
        <h3>About</h3>
    </div>
</div>
          
          
  </div>
</div>
<div class="cntmain">
  <div class="container">
    <div class=" whitearea aboutpage">
     
      <div class="row abouttop">
       <div class="col-sm-4">
        <div class="aboutimg">
          <img src="<?=base_url();?>img/about.jpg"  class="img-responsive" alt=""/> </div>
       </div>
       <div class="col-sm-8">
        <div class="aboutright">
         <p>Five years experience in the industry and with a wide range of high quality packaging solutions and disposables that cater to hotels, restaurants, hospitals, airlines and many more has helped establish Sahara Trading LLC as one of the more prominent suppliers in the UAE. With a dedicated customer service and sales team our customers can be assured of highest standards of service. A highly efficient delivery team with well stocked warehouses ensures on-time delivery all over UAE.
             <br>
             <br>
With the innovative products of over 50 items from aluminum foil, containers, plastic wraps, bags, fancy glasses, Drinking Straws, Paper cup to the customized products for the different market segments across the globe. Dispose  is an integrated manufacturer and distributor of disposable range of plastic, paper, aluminum.  Self-driven, we offer exceptional quality products which make us a world-class manufacture We have the capacity to fill large-scale orders for large and well-established companies.. Whatever you need, we would like to assist you.
</p>
        
        </div>
       </div>
      
      </div>
     
     

    </div>
    <div class="aboutbottom">
     <div class="vision clearfix">
      <div class="visionleft"><div class="misionimg"><img src="<?=base_url();?>img/vision.png"  class="img-responsive"  alt=""/></div></div>
      <div class="visionright">
      <h4>Vision</h4>
        <p> Ensure , empower and motivate all the employees to foster a culture of innovation and continual improvement within the organization to create a world class organization. </p>
      </div> 
      
     </div>
     <div class="mission clearfix">
     <div class="missionright">
       <div class="missionimg"><img src="<?=base_url();?>img/mission.png"  class="img-responsive"  alt=""/></div>
      </div>
      <div class="missionleft">
       <h4>Mission</h4>
       <p> To differentiate our product in a crowded marketplace, while offering the benefit of a cost-effective and easy-to-store package. </p>
      </div>
      
     </div>
    
    </div>
    
    
  </div>
  
</div>