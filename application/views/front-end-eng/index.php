<div id="bannermain">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div class="navimain">
          <div class="navigationarea hidden-xs"> <span class="menuicon"></span>
            <ul class="clearfix">
              <li class="activepage"><a href="<?=site_url();?>">HOME</a></li>
              <li><a href="<?=site_url();?>about">About Us</a></li>
              <li><a href="<?=site_url();?>products">PRODUCTS</a></li>
              <li><a href="<?=site_url();?>news-event">News and Events</a></li>
              <li><a href="<?=site_url();?>career">Career</a></li>
              <li><a href="<?=site_url();?>contact">Contact Us</a></li>
              <a href="<?=base_url().CATALOGUE?>" class="cataloguebtn"><span></span>CATALOGUE</a>
            </ul>
          </div>
          <nav class="navbar visible-xs" role="navigation"> 
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a href="<?=base_url();?>index.php" class="logo"></a></div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav navbar-right">
                <li class="activepage"><a href="<?=site_url();?>">Home</a></li>
                <li><a href="<?=site_url();?>about">About</a></li>
                 <li><a href="<?=site_url();?>products">PRODUCTS</a></li>
              <li><a href="<?=site_url();?>news-event">News and Events</a></li>
              <li><a href="<?=site_url();?>career">Career</a></li>
              <li><a href="<?=site_url();?>contact">Contact Us</a></li>
              <li><a href="<?=base_url().CATALOGUE?>">CATALOGUE</a></li>
              </ul>
            </div>
            <!-- /.navbar-collapse --> 
          </nav>
        </div>
      </div>
      <div class="col-sm-8">
        <div id="banner">
          <div class="bannermain">
            <div id="indicators_wrapper">
              <div id="indicators"></div>
            </div>
            <div id="text" class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="cntmain">
  <div class="container">
    <div class="welcomearea">
      <div class="row">
        <div class="col-sm-4">
          <div class="welcomelogo"><img src="<?=base_url();?>img/welcomeimg.jpg" class="img-responsive" alt=""/></div>
        </div>
        <div class="col-sm-8">
          <h1>Welcome to <span>Dispoze</span></h1>
          <p>Five years experience in the industry and with a wide range of high quality packaging solutions and disposables that cater to hotels, restaurants, hospitals, airlines and many more has helped establish Sahara Trading LLC as one of the more prominent suppliers in the UAE. With a dedicated customer service and sales team our customers can be assured of highest standards of service. A highly efficient delivery team with well stocked warehouses ensures on-time delivery all over UAE.</p>
          <a href="<?=site_url();?>/about" class="more">Rread More</a> </div>
      </div>
    </div>
    <div class="newarrivals-area">
      <h3>New Arrivals </h3>
      <div class="newarrivals">
        <ul class="clearfix">
            <?php if(count($products>0)){
                foreach ($products as $product){?>
          <li>
            <div class="imgbox"><img src="<?=base_url();?><?=$product['image'];?>" class="img-responsive" alt=""/></div>
          </li>
            <?php } }?>
          <li> <a href="<?=site_url();?>/products"" class="morebtn"> <span> More <br/>
            Products </span> </a> </li>
        </ul>
      </div>
    </div>
    
    <div class="homebottom clearfix">
     <div class="col-sm-5">
      <div class="newshome">
       <h4>News and Events</h4>
          <div class="newsslide clearfix">
                    <div id="nav-controls"></div>
                    <div class="owl-carousel" id="newsslide">
                        <?php if(count($news >0)){
                        foreach ($news as $newNews){?>
                        <div class="item">
                          <div class="newsdate">
                          <span><?=$newNews['day'];?></span><?=$newNews['month']." ".$newNews['year'];?>
                          </div>
                          <p><?= $newNews['title'];?></p>
                        </div>
                        <?php }} ?>
                    </div>
          </div>
      </div> 
     </div>
     <div class="col-sm-7">
      <div class="testimonialshome">
       <h4>Testimonials </h4>
       
       <div class="testimonialslide clearfix">
                   
                    <div class="owl-carousel" id="testimonialslide">
                        <div class="item">
                         <div class="testitext">  
                          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,  </p>
                          <div class="testiname"><span>Ram Gopal |</span> CEO ABC Group</div>
                          
                          </div>
                        </div>
                        <div class="item">
                          <div class="testitext">  
                          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took ... </p>
                          <div class="testiname"><span>Dinesh Kumar |</span> MD BBD Group</div>
                          </div>
                          
                        </div>
                        <div class="item">
                          <div class="testitext">  
                          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took ... </p>
                          <div class="testiname"><span>Shameer p h|</span> Manager Team lmc</div>
                          </div>
                          
                        </div>
                    </div>
                </div>
       
       
      </div>
     </div>
    </div>
    
    
  </div>
</div>