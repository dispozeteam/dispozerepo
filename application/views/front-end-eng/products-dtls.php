        <div class="innerbanner">
            <div class="innertitle">
            <h3>Products</h3>
            </div>
        </div>
  </div>
</div>
<div class="cntmain">
  <div class="container">
    <div class="productmain-dtls">
      <div class="row">
          <?php if(count($products>0)){
              foreach ($products as $product){
              ?>
           <div class="col-sm-3">
                <div class="productdtlsleft">
                    <span>
                    <img src="<?=base_url().$product->image;?>"  class="img-responsive"  alt=""/>
                    </span>
                </div>
           </div>
           <div class="col-sm-9">
               <div class="productdtlsright">
                <h4><?=$product->product;?></h4>
                <?php 
                $des=$product->description;
                $pname=$product->description;
                $ptype=$product->product_type;
                $ppaking=$product->packing;
                $pcolor=$product->color;
                $pmetirials=$product->meterial;
                $pitem_size=$product->item_size;
                $pgsweight=$product->gsweight;
                $pcapacity=$product->capacity;
                $pthickness=$product->thickness;
                $pimage=$product->image;
                ?>
                <?php if($des!="") {?>
                <div class="row">
                  <div class="col-sm-12"><?= $des?></div>
                  
                </div>
                <?php }?>

                <div class="containerforfutures">
                <?php if($ptype!="") {?>
                <div class="row">
                  <div class="col-sm-2">Product Type:</div>
                  <div class="col-sm-10"><?= $ptype?></div>
                </div>
                <?php }?>

                <?php if($ppaking!="") {?>
                <div class="row">
                  <div class="col-sm-2">Packing:</div>
                  <div class="col-sm-10"><?=$ppaking?></div>
                </div>
                <?php }?>

                <?php if($pcolor!="") {?>
                <div class="row">
                  <div class="col-sm-2">Color:</div>
                  <div class="col-sm-10"><?=$pcolor?></div>
                </div>
                <?php }?>

                <?php if($pmetirials!="") {?>
                <div class="row">
                  <div class="col-sm-2">Materials:</div>
                  <div class="col-sm-10"><?=$pmetirials?></div>
                </div>
                <?php }?>

                 <?php if($pitem_size!="") {?>
                <div class="row">
                  <div class="col-sm-2">Item Size:</div>
                  <div class="col-sm-10"><?=$pitem_size?></div>
                </div>
                <?php }?>

                <?php if($pgsweight!="") {?>
                <div class="row">
                  <div class="col-sm-2">Gross weight:</div>
                  <div class="col-sm-10"><?=$pgsweight?></div>
                </div>
                <?php }?>

                 <?php if($pcapacity!="") {?>
                <div class="row">
                  <div class="col-sm-2">Capacity:</div>
                  <div class="col-sm-10"><?=$pcapacity?></div>
                </div>
                <?php }?>

                 <?php if($pthickness!="") {?>
                <div class="row">
                  <div class="col-sm-2">Thickness:</div>
                  <div class="col-sm-10"><?=$pthickness?></div>
                </div>
                <?php }?>
                </div>
                <a href="tel:+91 568941235" class="callus"> Call us </a>
               </div>
           </div>
          <?php } } ?>
      </div>
    </div>
  </div>
</div>
