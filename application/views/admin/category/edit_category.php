<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Edit Product Category
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Product Category</h3>
                    </div>
                    <div class="box-body">
                      <form action="<?php echo site_url(); ?>/update_category" method="post">
                      
                  	<?php foreach($category as $r) { ?>
                  	<input type="hidden" name="editId" value="<?php echo $r['pcat_id']; ?>"/>
	                        <div class="row">			
	                            <div class="col-lg-4 col-md-5 col-sm-5">
	                                
	                                    <div class="form-group">
	                                        <label for="category">Category (English)</label><span class="text-danger">*</span>
	                                        <input type="text" name="category" id="category" class="form-control" value="<?php echo $r['cat_english']; ?>" required>
	                                    </div>
	                            </div>
	                            <div class="col-lg-4 col-md-5 col-sm-5">
	                                    <div class="form-group">
	                                        <label for="categoryArabic">Category (Arabic)</label>
	                                        <input type="text" name="categoryArabic" id="categoryArabic" class="form-control" value="<?php echo $r['cat_arab']; ?>" dir="rtl">
	                                    </div>
	                            </div>
	                        </div>
	                       	
	                       	<div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Update</button>
                            </div>
                        <?php } ?>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
