<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Product Category
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Product Category</h3>
                    </div>
                    <div class="box-body">
                      <form action="<?php echo site_url(); ?>insert_category" method="post">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                    <div class="form-group">
                                        <label for="category">Category (English)</label><span class="text-danger">*</span>
                                        <input type="text" name="category" id="category" class="form-control" required>
                                    </div>  
                                   
                            </div>
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                    <div class="form-group">
                                        <label for="categoryArabic">Category(Arabic)</label>
                                        <input type="text" name="categoryArabic" id="categoryArabic" class="form-control" dir="rtl">
                                    </div>  
                            </div>
                        </div>
                        	<div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                            </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
