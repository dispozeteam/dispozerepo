<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Product Category
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Categories</h3>
                        <!-- for seession message -->
                        <?php if($this->session->flashdata('flash')) { ?>
                        	<div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                			<?= $this->session->flashdata('flash')['message']; ?>
            				</div>
                        <?php } ?>
                        
                        <span class="pull-right"><a href="<?php echo site_url(); ?>category" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Product Category</th>
                                <th>Product Category (Arabic)</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            <?php 
                            $i = $this->uri->segment(3);
                            
                            if(count($records)=="") { ?>
								<tr>
									<td colspan="3" align="center"><h4>No data found.!</h4></td>
								</tr>	
							<?php } else {
								
                            foreach($records as $r) {
                            //table ID
                            $tab_id = $r->pcat_id;	
							$i++;
                            ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $r->cat_english; ?></td>
                                        <td><?= $r->cat_arab; ?></td>
                                        <td>
                                        	<a href="<?php echo site_url(); ?>edit_category/<?php echo $tab_id; ?>" class="btn btn-success btn-flat">Edit</a>
                                            <a href="<?php echo site_url(); ?>delete_category/<?php echo $tab_id; ?>" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a>  
                                        </td>
                                    </tr>
                                  <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $rowCount = count($records); ?>
                    <!--for pagination --->
					<div class="row" align="center">
						<?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
					</div>
					<!-- pagination end -->
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script>	
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>