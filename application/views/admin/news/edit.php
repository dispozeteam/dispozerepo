<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            News & Events
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Update News & Events</h3>
                    </div>
                    <div class="box-body">
                        <?php foreach ($news as $key){?>
                        <form action="<?php echo site_url(); ?>/news/edit/" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <input type="hidden" name="editId" id="editId" value="<?= $key->id;?>">
                                    <div class="form-group">
                                        <label for="title">Title<span style="color: #CC0000" >*</span></label>
                                        <input type="text" name="title" id="title" class="form-control" required value="<?= $key->title;?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea type="text" name="description" id="description" class="form-control"  ><?= $key->description;?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="date">Date<span style="color: #CC0000" ></span></label>
                                        <input type="text" name="date" id="date" class="form-control datepicker" value="<?php if($key->news_date!='0000-00-00') { echo date('d-m-Y', strtotime($key->news_date)); } ?>"  />
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label for="titlearb">Title in Arabi<span style="color: #CC0000" >*</span></label>
                                        <input type="text" name="titlearb" id="titlearb" class="form-control" value="<?= $key->titleArabi;?>" required />
                                    </div>
                                    <div class="form-group">
                                        <label for="descriptionarb">Description in Arabi</label>
                                        <textarea type="text" name="descriptionarb" id="descriptionarb" class="form-control"  ><?= $key->descriptionArabi;?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                            </div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
