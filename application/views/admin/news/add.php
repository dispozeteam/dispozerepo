<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>News
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add  News & Events </h3>
                    </div>
                    <div class="box-body">
                    	<form action="<?php echo site_url(); ?>/news/add" enctype="multipart/form-data" method="post">
                         <div class="row">
                             <div class="col-lg-6 col-md-6 col-sm-6">
                                 <div class="form-group">
                                     <label for="title">Title<span style="color: #CC0000" >*</span></label>
                                     <input type="text" name="title" id="title" class="form-control" required />
                                 </div>
                                 <div class="form-group">
                                     <label for="description">Description</label>
                                     <textarea type="text" name="description" id="description" class="form-control"  ></textarea>
                                 </div>
                                 <div class="form-group">
                                     <label for="date">Date<span style="color: #CC0000" ></span></label>
                                     <input type="text" name="date" id="date" class="form-control datepicker"  />
                                 </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-6">
                                 <div class="form-group">
                                     <label for="titlearb">Title in Arabi<span style="color: #CC0000" >*</span></label>
                                     <input type="text" name="titlearb" id="titlearb" class="form-control" required />
                                 </div>
                                 <div class="form-group">
                                     <label for="descriptionarb">Description in Arabi</label>
                                     <textarea type="text" name="descriptionarb" id="descriptionarb" class="form-control"  ></textarea>
                                 </div>
                             </div>
                         </div>
                         <div class="form-group">
                             <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                         </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
