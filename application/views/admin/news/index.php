<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            News & Events
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of News & Events</h3>
                        <!-- for seession message -->
                        <?php if($this->session->flashdata('flash')) { ?>
                            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                                <?= $this->session->flashdata('flash')['message']; ?>
                            </div>
                        <?php } ?>
                        <span class="pull-right"><a href="<?php echo site_url();?>/news/add" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Date</th>
                                <th style="width: 150px;">Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = $this->uri->segment(3);
							if(count($news)>0){
                                foreach($news as $r) {
                                    $tab_id = $r['id'];
                            $i++;
                                ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $r['title']; ?></td>
                                        <td><?= $r['description']; ?></td>
                                        <td><?php if($r['news_date']!='0000-00-00'){ echo date('d-m-Y', strtotime($r['news_date'])); } ?></td>
                                        <td>
                                            <a href="<?= site_url(); ?>/news/delete/<?= $tab_id; ?>" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a>
                                            <a href="<?= site_url(); ?>/news/editview/<?= $tab_id; ?>" class="btn btn-success btn-flat">Edit</a>
                                        </td>

                                    </tr>
                                <?php }
							} else {
							    ?>
                                <tr><td colspan="5" align="center">News & Events list is empty</td></tr>
                                <?php
                            } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $rowCount = count($news);?>
                    <!--for pagination --->
                    <div class="row" align="center">
                        <?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
                    </div>
                    <!-- pagination end -->
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    function delete_type()
    {
        var del=confirm("Do you Want to Delete ?");
        if(del==true)
        {
            window.submit();
        }
        else
        {
            return false;
        }
    }
</script>