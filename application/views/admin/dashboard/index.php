<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12"><h4>Welcome to Dispose admin panel</h4></div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                        <h3><?php echo $product ?></h3>
                        <p>Products</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                   <a href="<?= site_url(); ?>/product" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
     
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                        <h3><?php echo $category; ?></h3>
                        <p>Category</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-cubes"></i>
                    </div>
                   <a href="<?= site_url(); ?>all_category" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
       
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                        <h3><?php echo $news; ?></h3>
                        <p>News</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-newspaper-o"></i>
                    </div>
                   <a href="<?= site_url(); ?>news" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        

     </section>
</div>
