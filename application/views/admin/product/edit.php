<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Product
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Update Product</h3>
                    </div>
                    <div class="box-body">
                        <?php foreach ($product as $key){?>
                        <form action="<?php echo site_url(); ?>/product/edit/" method="post" enctype="multipart/form-data" onsubmit="return validateForm();"
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <input type="hidden" name="editId" id="editId" value="<?= $key->id;?>">
                                    <div class="form-group">
                                        <label for="title">Product<span style="color: #CC0000" >*</span></label>
                                        <input type="text" name="product" id="product" class="form-control" required value="<?= $key->product;?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea type="text" name="description" id="description" class="form-control"  ><?= $key->description;?></textarea>
                                    </div>
                                    <div class="form-group">
                                     <label for="product_type">Product Type</label>
                                     <input  type="text" name="product_type" id="product_type" class="form-control" value="<?=$key->product_type;?>"  >
                                 </div>
                                 <div class="form-group">
                                     <label for="packing">Packing</label>
                                     <input  type="text" name="packing" id="packing" class="form-control" value="<?=$key->packing;?>">
                                 </div>
                                   <div class="form-group">
                                     <label for="color">Color</label>
                                     <input  type="text" name="color" id="color" class="form-control" value="<?=$key->color;?>">
                                 </div>
                                 <div class="form-group">
                                     <label for="meterial">Material</label>
                                     <input  type="text" name="meterial" id="meterial" class="form-control" value="<?=$key->meterial;?>">
                                 </div>
                                  <div class="form-group">
                                     <label for="item_size">Item code & Size</label>
                                     <input  type="text" name="item_size" id="item_size" class="form-control" value="<?=$key->item_size;?>">
                                 </div>
                                   
                                   <div class="form-group">
                                     <label for="gsweight">Gross Weight</label>
                                     <input  type="text" name="gsweight" id="gsweight" class="form-control" value="<?=$key->gsweight;?>">
                                 </div>
                                 <div class="form-group">
                                     <label for="capacity">Capacity</label>
                                     <input  type="text" name="capacity" id="capacity" class="form-control" value="<?=$key->capacity;?>">
                                 </div>
                                 <div class="form-group">
                                     <label for="thickness">Thickness</label>
                                     <input  type="text" name="thickness" id="thickness" class="form-control" value="<?=$key->thickness;?>">
                                 </div>
                                    <div class="form-group">
                                        <label for="file">Upload Image(285*226)<span style="color: #CC0000" >*</span></label>
                                        <input type="file" name="file" id="file" />
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">

                                    <div class="form-group">
                                        <label for="productarb">Product in Arabi<span style="color: #CC0000" >*</span></label>
                                        <input type="text" name="productarb" id="productarb" class="form-control" required value="<?= $key->productArabi;?>" />
                                    </div>

                                    <div class="form-group">
                                        <label for="descriptionarb">Description in Arabi</label>
                                        <textarea type="text" name="descriptionarb" id="descriptionarb" class="form-control"  ><?= $key->descriptionArabi;?></textarea>
                                    </div>
                                    <div class="form-group">
                                     <label for="product_typearabic">Product Type Arabi</label>
                                     <input  type="text" name="product_typearabic" id="product_typearabic" class="form-control" value="<?=$key->product_typearabic;?>">
                                 </div>
                                 <div class="form-group">
                                     <label for="packingarabi">Packing Arabi</label>
                                     <input  type="text" name="packingarabi" id="packingarabi" class="form-control" value="<?=$key->packingarabi;?>">
                                 </div>
                                 <div class="form-group">
                                     <label for="color_arabi">Color Arabi</label>
                                     <input  type="text" name="color_arabi" id="color_arabi" class="form-control" value="<?=$key->color_arabi;?>">
                                 </div>
                                 <div class="form-group">
                                     <label for="meterial_arabi">Material Arabi</label>
                                     <input  type="text" name="meterial_arabi" id="meterial_arabi" class="form-control" value="<?=$key->meterial_arabi;?>">
                                 </div>

                                </div>

                            </div>
                            <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                    <div class="form-group">
                                        <label for="category">Category</label><span class="text-danger">*</span>
                                        <ul class="category_combo_list">
                                            <?php
                                                $i=0;
                                                $catMapCatId = array();
                                                foreach($mappcat as $cM) {
                                                    $catMapId = $cM->p_mapID; //echo $catMapId;
                                                    $catMapCatId[$i] = $cM->pcat_id;
                                                    $i++;
                                                 }
                                                foreach($allcategory as $cat)
                                                {
                                                    $catTableId = $cat->pcat_id;
                                            ?>
                                            <li><input type="checkbox" name="proCategoryId[]" value="<?= $cat->pcat_id; ?>" <?php for($i=0;$i<count($catMapCatId);$i++) { if($catTableId==$catMapCatId[$i]) { echo "checked" ; } } ?> />&nbsp;&nbsp;<?= $cat->cat_english.' '.'('.$cat->cat_arab.')' ?></li>
                                            <?php } ?>
                                        </ul>
                                    </div>  
                                   
                            </div>
                        </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                            </div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    

function validateForm()
{
 checked = $("input[type=checkbox]:checked").length;
  if (!checked) {
    alert("Please select product category");
    return false;
  }
}

</script>
