<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>Product
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Product </h3>
                    </div>
                    <div class="box-body">
                    	<form action="<?php echo site_url(); ?>/product/add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();">
                         <div class="row">
                             <div class="col-lg-6 col-md-6 col-sm-6">
                                 <div class="form-group">
                                     <label for="product">Product<span style="color: #CC0000" >*</span></label>
                                     <input type="text" name="product" id="product" class="form-control" required />
                                 </div>
                                 <div class="form-group">
                                     <label for="description">Description</label>
                                     <textarea type="text" name="description" id="description" class="form-control"  ></textarea>
                                 </div>
                                 <div class="form-group">
                                     <label for="product_type">Product Type</label>
                                     <input  type="text" name="product_type" id="product_type" class="form-control"  >
                                 </div>
                                 <div class="form-group">
                                     <label for="packing">Packing</label>
                                     <input  type="text" name="packing" id="packing" class="form-control"  >
                                 </div>
                                   <div class="form-group">
                                     <label for="color">Color</label>
                                     <input  type="text" name="color" id="color" class="form-control"  >
                                 </div>
                                 <div class="form-group">
                                     <label for="meterial">Material</label>
                                     <input  type="text" name="meterial" id="meterial" class="form-control"  >
                                 </div>
                                  <div class="form-group">
                                     <label for="item_size">Item code & Size</label>
                                     <input  type="text" name="item_size" id="item_size" class="form-control"  >
                                 </div>
                                   
                                   <div class="form-group">
                                     <label for="gsweight">Gross Weight</label>
                                     <input  type="text" name="gsweight" id="gsweight" class="form-control"  >
                                 </div>
                                 <div class="form-group">
                                     <label for="capacity">Capacity</label>
                                     <input  type="text" name="capacity" id="capacity" class="form-control"  >
                                 </div>
                                 <div class="form-group">
                                     <label for="thickness">Thickness</label>
                                     <input  type="text" name="thickness" id="thickness" class="form-control"  >
                                 </div>

                                 <div class="form-group">
                                     <label for="file">Upload Image(285*226)<span style="color: #CC0000" >*</span></label>
                                     <input type="file" name="file" id="file"  required/>
                                 </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-6">
                                 <div class="form-group">
                                     <label for="productarb">Product in Arabi<span style="color: #CC0000" >*</span></label>
                                     <input type="text" name="productarb" id="productarb" class="form-control" required />
                                 </div>
                                 <div class="form-group">
                                     <label for="descriptionarb">Description in Arabi</label>
                                     <textarea type="text" name="descriptionarb" id="descriptionarb" class="form-control"  ></textarea>
                                 </div>
                                 <div class="form-group">
                                     <label for="product_typearabic">Product Type Arabi</label>
                                     <input  type="text" name="product_typearabic" id="product_typearabic" class="form-control"  >
                                 </div>
                                 <div class="form-group">
                                     <label for="packingarabi">Packing Arabi</label>
                                     <input  type="text" name="packingarabi" id="packingarabi" class="form-control"  >
                                 </div>
                                 <div class="form-group">
                                     <label for="color_arabi">Color Arabi</label>
                                     <input  type="text" name="color_arabi" id="color_arabi" class="form-control"  >
                                 </div>
                                 <div class="form-group">
                                     <label for="meterial_arabi">Material Arabi</label>
                                     <input  type="text" name="meterial_arabi" id="meterial_arabi" class="form-control"  >
                                 </div>
                             </div>
                         </div>
                          <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-5">
                                    <div class="form-group">
                                        <label for="category">Category</label><span class="text-danger">*</span>
                                        <ul class="category_combo_list">
                                            <?php
                                                foreach($category as $cat)
                                                {
                                            ?>
                                            <li><input type="checkbox" name="proCategoryId[]" value="<?= $cat->pcat_id; ?>" />&nbsp;&nbsp;<?= $cat->cat_english.' '.'('.$cat->cat_arab.')'; ?></li>
                                            <?php } ?>
                                        </ul>
                                    </div>  
                                   
                            </div>
                        </div>
                         <div class="form-group">
                             <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                         </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
    

function validateForm()
{
 checked = $("input[type=checkbox]:checked").length;
  if (!checked) {
    alert("Please select product category");
    return false;
  }
}

</script>