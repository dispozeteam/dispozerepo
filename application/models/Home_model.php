<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 21-04-2017
 * Time: 12:11 PM
 */
class Home_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function category_count(){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_CATEGORY);
        $query = $this->db->get();
        return $query->num_rows();
    }
     public function news_count(){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_NEWS);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function product_count(){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_PRODUCT);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function feedback($feedback){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_FEEDBACK);
        $this->db->LIKE('feedback',$feedback);
        $query = $this->db->get();
        return $query->num_rows();
    }
}