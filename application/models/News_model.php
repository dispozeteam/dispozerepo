<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 24-04-2017
 * Time: 03:12 PM
 */
class News_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function numofRows(){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_NEWS);
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function get_all_product($limit=null,$offset=null){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_NEWS);
        $this->db->ORDER_BY("id", "desc");
        $this->db->LIMIT($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function insertData($data){
        $query = $this->db->insert(TABLE_NEWS,$data);
        return $query;
    }

    public function getByCondition($data){
        $query = $this->db->get_where(TABLE_NEWS,$data);
        return $query->result();
    }

    public function updateData($data,$editId){
        $this->db->WHERE('id',$editId);
        return $this->db->UPDATE(TABLE_NEWS,$data);
    }

    public function deleteData($deleteId){
        $this->db->where('id',$deleteId);
        $query = $this->db->delete(TABLE_NEWS);
        return $query;
    }
}