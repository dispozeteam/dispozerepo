<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 25-04-2017
 * Time: 05:04 PM
 */
class Product_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function numofRows(){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_PRODUCT);
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function get_all_product($limit=null,$offset=null){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_PRODUCT);
        $this->db->ORDER_BY("id", "desc");
        $this->db->LIMIT($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function insertData($data){
        $query = $this->db->insert(TABLE_PRODUCT,$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function insertcatproduct($data2){
        $result = $this->db->insert(TABLE_PRODUCT_MAPPING,$data2);
        return $result;
    }
    public function deleteUpdatecat($deleteId){
        $this->db->where('product_id',$deleteId);
        $deleted=$this->db->delete(TABLE_PRODUCT_MAPPING);
    }
    public function updateMapping($dataUp){
        $result = $this->db->insert(TABLE_PRODUCT_MAPPING,$dataUp);
        return $result;
    }
    

    public function getAllMapCatData_product($editId){
        $this->db->select(TABLE_PRODUCT_MAPPING.'.*');
        $this->db->from(TABLE_PRODUCT_MAPPING);
        $this->db->where(TABLE_PRODUCT_MAPPING.'.product_id='.$editId);
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    }

    public function getByCondition($data){
        $query = $this->db->get_where(TABLE_PRODUCT,$data);
        return $query->result();
    }

    public function updateData($data,$editId){
        $this->db->WHERE('id',$editId);
        return $this->db->UPDATE(TABLE_PRODUCT,$data);

    }

    //for unlinking the prodcut image while deleting the Data
    public function unlinkProImage($id) {
        $this->db->SELECT('image');
        $this->db->WHERE('id',$id);
        $this->db->FROM(TABLE_PRODUCT);
        $query = $this->db->get();
        return $query->result();
    }

    public function deleteData($deleteId){
        $this->db->where('id',$deleteId);
        $query = $this->db->delete(TABLE_PRODUCT);
        return $query;
    }
    public function insert_cat($data){
        $query = $this->db->insert(TABLE_CATEGORY,$data);
        return $query;
    }
      public function all_cat($limit=null,$offset=null){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_CATEGORY);
        $this->db->ORDER_BY("id", "desc");
        $this->db->LIMIT($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getAllCategoryData($limit,$offset)
    {
        $this->db->select(TABLE_CATEGORY.'.*');
        $this->db->from(TABLE_CATEGORY);
        $this->db->order_by(TABLE_CATEGORY.'.pcat_id','desc');
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
    }

 public function delete_category($deleteId){
    $this->db->where('pcat_id',$deleteId);
        $query = $this->db->delete(TABLE_CATEGORY);
        return $query;
 }

 public function edit_category($deleteId){

        $this->db->SELECT('*');
        $this->db->FROM(TABLE_CATEGORY);
        $this->db->WHERE("pcat_id",$deleteId);
        $query = $this->db->get();
        return $query->result_array();
    }
 public function update_category($editId,$data){

    $this->db->WHERE('pcat_id',$editId);
    return $this->db->UPDATE(TABLE_CATEGORY,$data);
 }

 public function getAllCategoryData_product(){
      $this->db->select(TABLE_CATEGORY.'.*');
        $this->db->from(TABLE_CATEGORY);
        $this->db->order_by(TABLE_CATEGORY.'.pcat_id','desc');
        $query = $this->db->get();
        return $query->result();
 }

 public function getAllCat($productid){

           $this->db->select(TABLE_CATEGORY.'.cat_english,'.TABLE_PRODUCT_MAPPING.'.*');
        $this->db->from(TABLE_PRODUCT_MAPPING);
        $this->db->join(TABLE_CATEGORY,TABLE_CATEGORY.'.pcat_id='.TABLE_PRODUCT_MAPPING.'.pcat_id');
        $this->db->where(TABLE_PRODUCT_MAPPING.'.product_id='.$productid);
        $query = $this->db->get();
        return $query->result_array();
    }


}