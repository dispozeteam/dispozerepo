<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 27-04-2017
 * Time: 12:26 PM
 */
class Front_Home extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Product_model');
        $this->load->model('News_model');
    }

    public function index(){
        $newsArray = array();
        $record['products'] = $this->Product_model->get_all_product('7',0);
        $news     = $this->News_model->get_all_product('3',0);
        foreach ($news as $key){
            $date = $key['news_date'];
            $stamp = strtotime($date);

            $newNews['id'] = $key['id'];
            $newNews['title'] = $key['title'];
            $newNews['description'] = $key['description'];
            $newNews['day'] =date("d", $stamp);
            $newNews['month'] =date("F", $stamp);
            $newNews['year'] =date("Y", $stamp);
            array_push($newsArray,$newNews);
        }
        $record['news'] = $newsArray;
        $this->load->view('front-end-eng/header.php');
        $this->load->view('front-end-eng/index.php',$record);
        $this->load->view('front-end-eng/footer.php');
    }

    public function index_for_arabi(){
        $newsArray = array();
        $record['products'] = $this->Product_model->get_all_product('7',0);
        $news     = $this->News_model->get_all_product('3',0);
        foreach ($news as $key){
            $date = $key['news_date'];
            $stamp = strtotime($date);

            $newNews['id']                  = $key['id'];
            $newNews['title']               = $key['title'];
            $newNews['titleArabi']          = $key['titleArabi'];
            $newNews['description']         = $key['description'];
            $newNews['descriptionArabi']    = $key['descriptionArabi'];
            $newNews['day']                 = date("d", $stamp);
            $newNews['month']               = date("F", $stamp);
            $newNews['year']                = date("Y", $stamp);
            array_push($newsArray,$newNews);
        }
        $record['news'] = $newsArray;
        $this->load->view('front-end-arb/header.php');
        $this->load->view('front-end-arb/index.php',$record);
        $this->load->view('front-end-arb/footer.php');
    }


}