<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 24-04-2017
 * Time: 02:30 PM
 */
class News extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('News_model');
        $this->load->library('pagination');
        $this->load->library('upload');

        //for session setting
        if(empty($this->session->userdata("user_id")))
        {
            $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out.!"]);
            redirect(site_url(),'refresh');
        }
    }

    public function index(){
        $num_rows_array = $this->News_model->numofRows();
        //$num_rows = count($num_rows_array);

        $config['base_url']     =  base_url().'index.php/news/index';
        $config['total_rows']   =  $num_rows_array;
        $config['per_page']     = '9';

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);
        $record['news'] = $this->News_model->get_all_product($config['per_page'],$this->uri->segment(3));

        $this->load->view('admin/header');
        $this->load->view('admin/news/index',$record);
        $this->load->view('admin/footer');
    }

    public function insert(){
        $this->form_validation->set_rules('title','Title','required');
        if($this->form_validation->run()===false){
            $this->load->view('admin/header');
            $this->load->view('admin/news/add');
            $this->load->view('admin/footer');
        }
        else{
            $date1 = $this->input->post('date');
            if(!$date1){
                $date = '';
            }
            else{
                $date = date('Y-m-d', strtotime($date1));
            }
            $data = array(
                'title'=>$this->input->post('title'),
                'titleArabi'=>$this->input->post('titlearb'),
                'description'=>$this->input->post('description'),
                'descriptionArabi'=>$this->input->post('descriptionarb'),
                'news_date'=>$date
            );

            $success = $this->News_model->insertData($data);
            if($success){
                $this->session->set_flashdata("flash",["type"=>"success","message"=>"Data inserted successfully"]);
                redirect('news');
            }
            else{
                $this->session->set_flashdata("flash",["type"=>'failed',"message"=>"Sorry! Some thing went wrong"]);
                redirect('news');
            }
        }
    }

    public function update_view(){
        $editId = $this->uri->segment(3);
        $data = array('id'=>$editId);
        $records['news'] = $this->News_model->getByCondition($data);

        $this->load->view('admin/header');
        $this->load->view('admin/news/edit',$records);
        $this->load->view('admin/footer');
    }

    public function update(){
        $this->form_validation->set_rules('title','Title','required');
        $editId = $this->input->post('editId');
        if($this->form_validation->run()===false){
            redirect('news/editview/'.$editId);
        }
        else{

            $date1 = $this->input->post('date');
            if(!$date1){
                $date = '';
            }
            else{
                $date = date('Y-m-d', strtotime($date1));
            }

            $data['title']          = $this->input->post('title');
            $data['titleArabi']     = $this->input->post('titlearb');
            $data['description']    = $this->input->post('description');
            $data['descriptionArabi']=$this->input->post('descriptionarb');
            $data['news_date']      = $date;

            $success = $this->News_model->updateData($data,$editId);
            if($success){
                $this->session->set_flashdata("flash",["type"=>"success","message"=>"Data update successfully"]);
                redirect('news');
            }
            else{
                $this->session->set_flashdata("flash",["type"=>'failed',"message"=>"Sorry! Some thing went wrong"]);
                redirect('news');
            }
        }

    }

    public function delete(){
        $deleteId = $this->uri->segment(3);
        $success = $this->News_model->deleteData($deleteId);
        if($success){

            $this->session->set_flashdata("flash",["type"=>"success","message"=>"Data deleted successfully"]);
            redirect('news');
        }
        else{
            $this->session->set_flashdata("flash",["type"=>'failed',"message"=>"Sorry! Some thing went wrong"]);
            redirect('news');
        }
    }
}