<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 28-04-2017
 * Time: 12:12 PM
 */
class Career_Site extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('upload');
    }

    public function index(){
        $this->load->view('front-end-eng/header.php');
        $this->load->view('front-end-eng/menu.php');
        $this->load->view('front-end-eng/career.php');
        $this->load->view('front-end-eng/footer.php');
    }
    public function index_for_arabi(){
        $this->load->view('front-end-arb/header.php');
        $this->load->view('front-end-arb/menu.php');
        $this->load->view('front-end-arb/career.php');
        $this->load->view('front-end-arb/footer.php');
    }

    public function send_mail(){
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('email','Email','required');
        $this->form_validation->set_rules('contact-no','Contact Number','required');
        $this->form_validation->set_rules('post','Post','required');
        if($this->form_validation->run()===false){
            redirect('contact');
        }
        else{
            $sendTo  = 'sujinabodhi@gmail.com';
            $name    = $this->input->post('name');
            $mailId  = $this->input->post('email');
            $contact = $this->input->post('contact-no');
            $post    = $this->input->post('post');
            $profile = $this->input->post('profile');

            $bodytext = " Name : ".$name."\r\n Phone : ".$contact."\r\n Email : ".$mailId."\r\n Applied Post: ".$post."\r\n Profile : ".$profile."\r\n";

            $this->email->from($mailId,$name);
            $this->email->to($sendTo);
            $this->email->subject('Message from Dispoze contact-us');
            $this->email->message('Name :'.$name);
            $this->email->message($bodytext);

            $fileUpload=array();
            $isUpload=FALSE;
            $image = '';
            $up_image=array(
                'upload_path'=>'./images/email/',
                'allowed_types'=>'docx|pdf',
                'encrypt_name'=>TRUE
            );
            $this->upload->initialize($up_image);

            if(!$this->upload->do_upload('file')){
                $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Sorry! Something went wrong"]);
                redirect(site_url(),'refresh');
}else{
                $fileUpload=$this->upload->data();
                $isUpload=TRUE;
                $image  	=	"images/email/".$fileUpload['file_name'];
                $this->email->attach($image);
            }

            $success = $this->email->send();
            if(file_exists($image)) {
                unlink($image);
            }
            if($success){

                $this->session->set_flashdata("flash",["type"=>"success","message"=>"Message sent successfully"]);
                redirect(site_url(),'refresh');
            }else{
                $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Sorry! Something went wrong"]);
                redirect(site_url(),'refresh');
            }
        }
    }
}