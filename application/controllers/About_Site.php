<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 27-04-2017
 * Time: 05:27 PM
 */
class About_Site extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
    }

    public function index(){
        $this->load->view('front-end-eng/header.php');
        $this->load->view('front-end-eng/menu.php');
        $this->load->view('front-end-eng/about.php');
        $this->load->view('front-end-eng/footer.php');
    }
    public function index_for_arabi(){
        $this->load->view('front-end-arb/header.php');
        $this->load->view('front-end-arb/menu.php');
        $this->load->view('front-end-arb/about.php');
        $this->load->view('front-end-arb/footer.php');
    }
}