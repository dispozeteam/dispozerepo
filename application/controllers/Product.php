<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 25-04-2017
 * Time: 04:22 PM
 */
class Product extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Product_model');
        $this->load->library('pagination');
        $this->load->library('upload');

        //for session setting
        if(empty($this->session->userdata("user_id")))
        {
            $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out.!"]);
            redirect(site_url(),'refresh');
        }
    }

    public function index(){
        $num_rows_array = $this->Product_model->numofRows();
        //$num_rows = count($num_rows_array);

        $config['base_url']     =  base_url().'index.php/product/index';
        $config['total_rows']   = $num_rows_array;
        $config['per_page']     = '9';

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);
        $record['product'] = $this->Product_model->get_all_product($config['per_page'],$this->uri->segment(3));

        $this->load->view('admin/header');
        $this->load->view('admin/product/index',$record);
        $this->load->view('admin/footer');
    }

    public function insert(){
        $this->form_validation->set_rules('product','Product','required');
        $checkedArray = $this->input->post('proCategoryId');
        if($this->form_validation->run()===false){
             $res['category'] = $this->Product_model->getAllCategoryData_product();
            $this->load->view('admin/header');
            $this->load->view('admin/product/add',$res);
            $this->load->view('admin/footer');
        }
        else{
            $fileUpload=array();
            $isUpload=FALSE;
            $image = '';
            $up_image=array(
                'upload_path'=>'./images/product/',
                'allowed_types'=>'jpg|jpeg|png|gif',
                'encrypt_name'=>TRUE
            );
            $this->upload->initialize($up_image);
            if($this->upload->do_upload('file')){
                $fileUpload=$this->upload->data();
                $isUpload=TRUE;
                $image  	=	"images/product/".$fileUpload['file_name'];
            }
            $data = array(
                'product'=>$this->input->post('product'),
                'productArabi'=>$this->input->post('productarb'),
                'description'=>$this->input->post('description'),
                'descriptionArabi'=>$this->input->post('descriptionarb'),
                'product_type'=>$this->input->post('product_type'),
                'packing'=>$this->input->post('packing'),
                'color'=>$this->input->post('color'),
                'meterial'=>$this->input->post('meterial'),
                'item_size'=>$this->input->post('item_size'),
                'gsweight'=>$this->input->post('gsweight'),
                'capacity'=>$this->input->post('capacity'),
                'thickness'=>$this->input->post('thickness'),
                'product_typearabic'=>$this->input->post('product_typearabic'),
                'packingarabi'=>$this->input->post('packingarabi'),
                'color_arabi'=>$this->input->post('color_arabi'),
                'meterial_arabi'=>$this->input->post('meterial_arabi'),
                'image'=>$image
            );
             $success = $this->Product_model->insertData($data);
            if(!empty($checkedArray)) 
                {

           
            for($i=0;$i<count($checkedArray);$i++)
                    {
                        $data2['pcat_id'] = $checkedArray[$i];
                        $data2['product_id'] = $success;
                        
                        $cat_success=$this->Product_model->insertcatproduct($data2);
                    }
            

            }
            
            if($success){
                $this->session->set_flashdata("flash",["type"=>"success","message"=>"Data inserted successfully"]);
                redirect('product');
            }
            else{
                $this->session->set_flashdata("flash",["type"=>'failed',"message"=>"Sorry! Some thing went wrong"]);
                redirect('product');
            }
        }
    }

    public function update_view(){
        $editId = $this->uri->segment(3);
        $data = array('id'=>$editId);
        $records['product'] = $this->Product_model->getByCondition($data);
        $records['allcategory'] = $this->Product_model->getAllCategoryData_product();
        $records['mappcat'] = $this->Product_model->getAllMapCatData_product($editId);


        $this->load->view('admin/header');
        $this->load->view('admin/product/edit',$records);
        $this->load->view('admin/footer');
    }

    public function update(){
        $this->form_validation->set_rules('product','Product','required');
        $checkedArray = $this->input->post('proCategoryId');

        $editId = $this->input->post('editId');
        if($this->form_validation->run()===false){
            redirect('product/editview/'.$editId);
        }
        else{
            $image = '';
            $fileUpload=array();
            $isUpload=FALSE;
            $up_image=array(
                'upload_path'=>'./images/product/',
                'allowed_types'=>'jpg|jpeg|png|gif',
                'encrypt_name'=>TRUE
            );
            $this->upload->initialize($up_image);
            if($this->upload->do_upload('file')){
                $fileUpload=$this->upload->data();
                $isUpload=TRUE;
                $image  	=	"images/product/".$fileUpload['file_name'];
            }
            $data['product']          = $this->input->post('product');
            $data['productArabi']     = $this->input->post('productarb');
            $data['description']      = $this->input->post('description');
            $data['descriptionArabi'] = $this->input->post('descriptionarb');
            $data['product_type'] = $this->input->post('product_type');
            $data['packing'] = $this->input->post('packing');
            $data['color'] = $this->input->post('color');
            $data['meterial'] = $this->input->post('meterial');
            $data['item_size'] = $this->input->post('item_size');
            $data['gsweight'] = $this->input->post('gsweight');
            $data['capacity'] = $this->input->post('capacity');
            $data['thickness'] = $this->input->post('thickness');
            $data['product_typearabic'] = $this->input->post('product_typearabic');
            $data['packingarabi'] = $this->input->post('packingarabi');
            $data['color_arabi'] = $this->input->post('color_arabi');
            $data['meterial_arabi'] = $this->input->post('meterial_arabi');

            if($image){
                $data['image']     = $image;
            }
            

           

            $deleteupdateMap=$this->Product_model->deleteUpdatecat($editId);
            
            for($i=0;$i<count($checkedArray);$i++)
                    {
            $dataUp['pcat_id']=$checkedArray[$i];
            $dataUp['product_id']=$editId;
            
            $updateMap=$this->Product_model->updateMapping($dataUp);
              }
           
            $success = $this->Product_model->updateData($data,$editId);
            
            if($success){
                $this->session->set_flashdata("flash",["type"=>"success","message"=>"Data update successfully"]);
                redirect('product');
            }
            else{
                $this->session->set_flashdata("flash",["type"=>'failed',"message"=>"Sorry! Some thing went wrong"]);
                redirect('product');
            }
        }

    }

    public function delete(){
        $deleteId = $this->uri->segment(3);
        $image = $this->Product_model->unlinkProImage($deleteId);
        if(isset($image)) {
            $newsImg = $image[0]->image;
            if(file_exists($newsImg)) {
                unlink($newsImg);
            }
        }

        $success = $this->Product_model->deleteData($deleteId);
        if($success){
            $deleteupdateMap=$this->Product_model->deleteUpdatecat($deleteId);
            
            $this->session->set_flashdata("flash",["type"=>"success","message"=>"Data deleted successfully"]);
            redirect('product');
        }
        else{
            $this->session->set_flashdata("flash",["type"=>'failed',"message"=>"Sorry! Some thing went wrong"]);
            redirect('product');
        }
    }

   public function category(){

        $this->load->view('admin/header');
        $this->load->view('admin/category/add_category.php');
        $this->load->view('admin/footer');
   }
   public function insert_category(){

      $data = array(
                'cat_english'=>$this->input->post('category'),
                'cat_arab'=>$this->input->post('categoryArabic')
                
            );
     $success = $this->Product_model->insert_cat($data);
     if($success){
        $this->session->set_flashdata("flash",["type"=>"success","message"=>"Data added successfully"]);
            redirect('all_category');
     }else{
            $this->session->set_flashdata("flash",["type"=>'failed',"message"=>"Sorry! Some thing went wrong"]);
            redirect('all_category');
        }

   }
   public function allcategory(){

             $num_rows = $this->db->count_all('tbl_category'); //echo $num_rows;die;
             $this->load->library('pagination');

             $config['base_url'] = base_url().'Product/allcategory';
             $config['total_rows'] = $num_rows;
             $config['per_page'] = 15;
            
             //$config['use_page_numbers'] = TRUE;
             $config['full_tag_open'] = "<ul class='pagination'>";
             $config['full_tag_close'] ="</ul>";
             $config['num_tag_open'] = '<li>';
             $config['num_tag_close'] = '</li>';
             $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
             $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
             $config['next_tag_open'] = "<li>";
             $config['next_tagl_close'] = "</li>";
             $config['prev_tag_open'] = "<li>";
             $config['prev_tagl_close'] = "</li>";
             $config['first_tag_open'] = "<li>";
             $config['first_tagl_close'] = "</li>";
             $config['last_tag_open'] = "<li>";
             $config['last_tagl_close'] = "</li>";
            
             $this->pagination->initialize($config);
            // ******* pagiantion configrtn ends *** /////////
             
             
             
             $res['records'] = $this->Product_model->getAllCategoryData($config['per_page'],$this->uri->segment(3));
             $this->load->view('admin/header');
             $this->load->view('admin/category/index',$res);
             $this->load->view('admin/footer');
   }

   public function delete_category(){
    $deleteId = $this->uri->segment(2);
    $success = $this->Product_model->delete_category($deleteId);
      if($success){
        $this->session->set_flashdata("flash",["type"=>"success","message"=>"Data deleted successfully"]);
            redirect('all_category');
     }else{
            $this->session->set_flashdata("flash",["type"=>'failed',"message"=>"Sorry! Some thing went wrong"]);
            redirect('all_category');
        }
   }

   public function edit_category(){
    $deleteId = $this->uri->segment(2);
    $records['category'] = $this->Product_model->edit_category($deleteId);

     $this->load->view('admin/header');
        $this->load->view('admin/category/edit_category',$records);
        $this->load->view('admin/footer');

   }
   public function update_category(){

     $data = array(
                'cat_english'=>$this->input->post('category'),
                'cat_arab'=>$this->input->post('categoryArabic')
            );
     $editId =$this->input->post('editId');
     $success=$this->Product_model->update_category($editId,$data);

     if($success){
                $this->session->set_flashdata("flash",["type"=>"success","message"=>"Data update successfully"]);
                redirect('all_category');
            }
            else{
                $this->session->set_flashdata("flash",["type"=>'failed',"message"=>"Sorry! Some thing went wrong"]);
                redirect('all_category');
            }
   }


}