<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 18-04-2017
 * Time: 01:30 PM
 */
class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Home_model');
        //for session setting
        if(empty($this->session->userdata("user_id")))
        {
            $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out.!"]);
            redirect(site_url(),'refresh');
        }
    }
    public function index(){
//        $record['category'] = $this->Home_model->category_count();
        $record['product'] = $this->Home_model->product_count();
        $record['category'] = $this->Home_model->category_count();
        $record['news'] = $this->Home_model->news_count();


        $this->load->view('admin/header');
        $this->load->view('admin/dashboard/index',$record);
        $this->load->view('admin/footer');
    }


}