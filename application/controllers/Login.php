<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 20-04-2017
 * Time: 03:21 PM
 */
class Login extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Home_model');
        $this->load->model('Login_model');
    }

    public function index(){
        $this->form_validation->set_rules('userName','Username','required');
        $this->form_validation->set_rules('password','Password','required');
        if($this->form_validation->run()===false){
            //$this->session->set_flashdata("flash", ["type" => "danger", "message" => "Enter all details..!"]);
            $this->load->view('admin/index');
        }
        else{
            $userName = $this->input->post('userName');
            $password = $this->input->post('password');//echo $userName.$password;die;
            $data     = $this->Login_model->resolve_user_login($userName, $password);
            if($data)
            {
                // set session user datas
                $_SESSION['user_id']      = $data->id;
                $_SESSION['user_type']    = $data->usertype;
                $_SESSION['user_name']    = $userName;
                $_SESSION['logged_in']    = (bool)true;
                $_SESSION['is_confirmed'] = (bool)true;

                redirect('home');

            } else {
                $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Wrong User Name or password!"]);
                $this->load->view('admin/index');
            }
        }
    }
    public function change_pass_view() {
        $this->load->view('admin/header');
        $this->load->view('admin/change_password/index');
        $this->load->view('admin/footer');
    }
    public function changePassword() {

        $oldPass = $this->input->post('password');
        $newPass = $this->input->post('nPassword');
        $confirmPass = $this->input->post('cPassword');

        //form validation
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('nPassword','New Password','required');
        $this->form_validation->set_rules('cPassword','Confirm Password','required');
        if ($this->form_validation->run() == false)
        {
            $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out.!"]);
            redirect('Login/change_pass_view');
        }
        else
        {
            if($newPass!=$confirmPass)
            {
                $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Password Mismatch.!"]);
                redirect('Login/change_pass_view');
            }
            else
            {
                $user = $this->Login_model->get_user($_SESSION['user_id']);
                $res = $this->Login_model->checkPasswordMatch($oldPass,$user->password);

                if($res)
                {
                    $updatePass = $this->Login_model->updatePassword($_SESSION['user_id'],$newPass);
                    if($updatePass) {
                        redirect('Login/logout');
                    }
                    else {
                        $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Something went wrong"]);
                        redirect('Login/index');
                    }
                }
                else
                {
                    $this->session->set_flashdata("flash", ["type" => "danger", "message" => "Current password is incorrect!"]);
                    redirect('Login/index');
                }
            }
        }

    }

    public function logout()
    {
        $user_data = $this->session->all_userdata(); //print_r($user_data);die;
        foreach ($user_data as $key => $value)
        {
            $this->session->unset_userdata($key);
        }
        $this->session->sess_destroy();
        redirect('admin');
    }
}