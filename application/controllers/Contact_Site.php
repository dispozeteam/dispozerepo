<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 28-04-2017
 * Time: 12:24 PM
 */
class Contact_Site extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('email');
    }

    public function index(){
        $this->load->view('front-end-eng/header.php');
        $this->load->view('front-end-eng/menu.php');
        $this->load->view('front-end-eng/contact.php');
        $this->load->view('front-end-eng/footer.php');
    }

    public function index_for_arabi(){
        $this->load->view('front-end-arb/header.php');
        $this->load->view('front-end-arb/menu.php');
        $this->load->view('front-end-arb/contact.php');
        $this->load->view('front-end-arb/footer.php');
    }
    public function send_mail(){
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('email','Email','required');
        $this->form_validation->set_rules('message','Message','required');
        if($this->form_validation->run()===false){
            redirect('contact');
        }
        else{
            $sendTo  = 'sujinabodhi@gmail.com';
            $name    = $this->input->post('name');
            $mailId  = $this->input->post('email');
            $contact = $this->input->post('contact-no');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');

            $bodytext = " Name : ".$name."\r\n Phone : ".$contact."\r\n Email : ".$mailId."\r\n Subject : ".$subject."\r\n Message : ".$message."\r\n";
            $this->email->from($mailId,$name);
            $this->email->to($sendTo);
            $this->email->subject('Message from Dispoze contact-us');
            $this->email->message($bodytext);

            $success = $this->email->send();
            if($success){
                $this->session->set_flashdata("flash",["type"=>"success","message"=>"Message sent successfully"]);
                redirect(site_url(),'refresh');
            }else{
                $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Sorry! Something went wrong"]);
                redirect(site_url(),'refresh');
            }
        }
    }
}