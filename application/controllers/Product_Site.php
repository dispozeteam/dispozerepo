<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 28-04-2017
 * Time: 12:04 PM
 */
class Product_Site extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Product_model');
    }

    public function index(){
            $data = array();
        $record['products'] = $this->Product_model->get_all_product();
        $record['category'] = $this->Product_model->getAllCategoryData_product();
        $product= $this->Product_model->get_all_product();
        $i=0;
        foreach ($product as $pr) 
        {
        $productid=$pr['id'];
        $product=$pr['product'];
        $product_img=$pr['image'];
        $productdesc=$pr['description'];

        $resultCat = $this->Product_model->getAllCat($productid);
        $catArray=array();
        foreach($resultCat as $rC)
            {
                array_push($catArray,$rC['cat_english']);
            }
        $subcat = implode(" ",$catArray);
        $item[$i]['id'] =$productid;
        $item[$i]['category1']=$subcat;
        $item[$i]['prd_name']=$product;
        $item[$i]['prd_img']=$product_img;
        $item[$i]['prd_description']=$productdesc;
        $i++;
       
        }
        $record['all_prd']=$item;   
        $this->load->view('front-end-eng/header.php');
        $this->load->view('front-end-eng/menu.php');
        $this->load->view('front-end-eng/products.php',$record);
        $this->load->view('front-end-eng/footer.php');
    }

    public function index_for_arabi(){
        $data = array();
        // $record['products'] = $this->Product_model->get_all_product();
        $record['category'] = $this->Product_model->getAllCategoryData_product();
        $product= $this->Product_model->get_all_product();
        $i=0;
        foreach ($product as $pr) 
        {
        $productid=$pr['id'];
        $product=$pr['productArabi'];
        $product_img=$pr['image'];
        $productdesc=$pr['descriptionArabi'];

        $resultCat = $this->Product_model->getAllCat($productid);
        $catArray=array();
        foreach($resultCat as $rC)
            {
                array_push($catArray,$rC['cat_english']);
            }
        $subcat = implode(" ",$catArray);
        $item[$i]['id'] =$productid;
        $item[$i]['category1']=$subcat;
        $item[$i]['prd_name']=$product;
        $item[$i]['prd_img']=$product_img;
        $item[$i]['prd_description']=$productdesc;
        $i++;
       
        }
        $record['all_prd']=$item;
        
        $this->load->view('front-end-arb/header.php');
        $this->load->view('front-end-arb/menu.php');
        $this->load->view('front-end-arb/products.php',$record);
        $this->load->view('front-end-arb/footer.php');
    }

    public function productsDetails(){
        $productId = $this->uri->segment(2);
        $data = array('id'=>$productId);
        $record['products'] = $this->Product_model->getByCondition($data);
        $this->load->view('front-end-eng/header.php');
        $this->load->view('front-end-eng/menu.php');
        $this->load->view('front-end-eng/products-dtls.php',$record);
        $this->load->view('front-end-eng/footer.php');
    }

    public function productsDetails_for_arabi(){
        $productId = $this->uri->segment(2);
        $data = array('id'=>$productId);
        $record['products'] = $this->Product_model->getByCondition($data);
        $this->load->view('front-end-arb/header.php');
        $this->load->view('front-end-arb/menu.php');
        $this->load->view('front-end-arb/products-dtls.php',$record);
        $this->load->view('front-end-arb/footer.php');
    }
}