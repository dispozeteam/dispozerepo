<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 28-04-2017
 * Time: 11:32 AM
 */
class News_Event_Site extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('News_model');
    }

    public function index(){
        $newsArray = array();
        $news     = $this->News_model->get_all_product();
        foreach ($news as $key){
            $date = $key['news_date'];
            $stamp = strtotime($date);

            $newNews['id'] = $key['id'];
            $newNews['title'] = $key['title'];
            $newNews['description'] = $key['description'];
            $newNews['day'] =date("d", $stamp);
            $newNews['month'] =date("F", $stamp);
            $newNews['year'] =date("Y", $stamp);
            array_push($newsArray,$newNews);
        }
        $record['news'] = $newsArray;

        $this->load->view('front-end-eng/header.php');
        $this->load->view('front-end-eng/menu.php');
        $this->load->view('front-end-eng/news.php',$record);
        $this->load->view('front-end-eng/footer.php');
    }

    public function index_for_arabi(){
        $newsArray = array();
        $news     = $this->News_model->get_all_product();
        foreach ($news as $key){
            $date = $key['news_date'];
            $stamp = strtotime($date);

            $newNews['id'] = $key['id'];
            $newNews['title'] = $key['title'];
            $newNews['titleArabi']  = $key['titleArabi'];
            $newNews['description'] = $key['description'];
            $newNews['descriptionArabi'] = $key['descriptionArabi'];
            $newNews['day'] =date("d", $stamp);
            $newNews['month'] =date("F", $stamp);
            $newNews['year'] =date("Y", $stamp);
            array_push($newsArray,$newNews);
        }
        $record['news'] = $newsArray;

        $this->load->view('front-end-arb/header.php');
        $this->load->view('front-end-arb/menu.php');
        $this->load->view('front-end-arb/news.php',$record);
        $this->load->view('front-end-arb/footer.php');
    }
}