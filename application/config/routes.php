<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Front_Home/';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// for front-end For English
$route['fronthome']             =   'Front_Home/index';
$route['about']                 =   'About_Site/index';
$route['career']                =   'Career_Site/index';
$route['career_mail']           =   'Career_Site/send_mail';
$route['contact']               =   'Contact_Site/index';
$route['contact_mail']          =   'Contact_Site/send_mail';
$route['news-event']            =   'News_Event_Site/index';
$route['products']              =   'Product_Site/index';
$route['products-dtls/(:any)']  =   'Product_Site/productsDetails/$i';

// for front-end For Arabi
$route['fronthomeArb']             =   'Front_Home/index_for_arabi';
$route['aboutArb']                 =   'About_Site/index_for_arabi';
$route['careerArb']                =   'Career_Site/index_for_arabi';
$route['career_mailArb']           =   'Career_Site/send_mail';
$route['contactArb']                  =   'Contact_Site/index_for_arabi';
$route['contact_mailArb']          =   'Contact_Site/send_mail';
$route['news-eventArb']            =   'News_Event_Site/index_for_arabi';
$route['productsArb']              =   'Product_Site/index_for_arabi';
$route['products-dtlsArb/(:any)']  =   'Product_Site/productsDetails_for_arabi/$i';

// for admin side
$route['admin'] = 'Login/index';
$route['home']  = 'Home/index';

$route['news/delete/(:any)']        = 'News/delete/$i';
$route['news/edit']                 = 'News/update';
$route['news/editview/(:any)']      = 'News/update_view/$i';
$route['news/add']                  = 'News/insert';
$route['news/(:any)']               = 'News/index/$i';

$route['product/delete/(:any)']        = 'Product/delete/$i';
$route['product/edit']                 = 'Product/update';
$route['product/editview/(:any)']      = 'Product/update_view/$i';
$route['product/add']                  = 'Product/insert';
$route['product/(:any)']               = 'Product/index/$i';
$route['category']               	   = 'Product/category';
$route['insert_category']              = 'Product/insert_category';
$route['all_category']                 = 'Product/allcategory';
$route['delete_category/(:any)']       = 'Product/delete_category/$i';
$route['edit_category/(:any)']         = 'Product/edit_category/$i';
$route['update_category']              = 'Product/update_category';





