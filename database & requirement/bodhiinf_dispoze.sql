-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 29, 2017 at 08:52 AM
-- Server version: 5.5.54-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bodhiinf_dispoze`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `pcat_id` int(11) NOT NULL,
  `cat_english` varchar(30) NOT NULL,
  `cat_arab` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`pcat_id`, `cat_english`, `cat_arab`) VALUES
(11, 'Paper-products', 'منتجات ورقية'),
(12, 'Foam-items', 'عناصر الرغوة'),
(13, 'Other-products', 'منتجات اخرى'),
(14, 'Aluminium-products', 'منتجات الألمنيوم'),
(15, 'Hygiene-products', 'منتجات النظافة'),
(17, 'Plastic Products', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `usertype` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`id`, `username`, `password`, `usertype`) VALUES
(1, 'admin', '$2y$10$WC5K4dPadimlDsMcb45VDOcbIaIc3yOTi6uI8z.CZaEdvU7iCks66', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `news_date` date NOT NULL,
  `titleArabi` text CHARACTER SET utf8 NOT NULL,
  `descriptionArabi` text CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`id`, `title`, `description`, `news_date`, `titleArabi`, `descriptionArabi`) VALUES
(16, 'Kitchen items 50% off', 'Buy now earn more than rs.1000/-', '2017-06-07', 'مستلزمات المطبخ خصم 50٪', 'شراء الآن كسب أكثر من rs.1000 / -'),
(17, 'Get Burger wrap ', '1000 pc. 70% off', '2017-05-01', 'الحصول على التفاف برغر', '1000 بيسي. 70٪ خصم');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `product` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `product_type` varchar(100) NOT NULL,
  `packing` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `meterial` varchar(100) NOT NULL,
  `item_size` varchar(100) NOT NULL,
  `gsweight` varchar(100) NOT NULL,
  `capacity` varchar(100) NOT NULL,
  `thickness` varchar(100) NOT NULL,
  `image` varchar(500) NOT NULL,
  `productArabi` text CHARACTER SET utf8 NOT NULL,
  `descriptionArabi` text CHARACTER SET utf8 NOT NULL,
  `product_typearabic` varchar(100) CHARACTER SET utf8 NOT NULL,
  `packingarabi` varchar(100) CHARACTER SET utf8 NOT NULL,
  `color_arabi` varchar(100) CHARACTER SET utf8 NOT NULL,
  `meterial_arabi` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `product`, `description`, `product_type`, `packing`, `color`, `meterial`, `item_size`, `gsweight`, `capacity`, `thickness`, `image`, `productArabi`, `descriptionArabi`, `product_typearabic`, `packingarabi`, `color_arabi`, `meterial_arabi`) VALUES
(27, 'PE GLOVES', 'Item: disposable polyethylene gloves, non-toxic, Environmental protection\r\nMateriel : Polyethylene resin', 'PE GLOVES', '100PCS/BAG, 100 BAGS/CARTON', 'CLEAR ', 'POLYETHYLENE RESIN', '', '', '', '', 'images/product/d61db1bd156ef83d8711fc73a427f047.jpg', 'بي غلوفيس', ' البيئة\r\nالعتاد: راتنج البولي إيثيلين', 'بي غلوفيس', '100 قطعة / الحقيبة، 100 حقيبة / الكرتون', 'واضح', 'Search Results  POLYETHYLENE RESIN Edit  راتيل بوليثيلين'),
(29, 'M.G. WHITE SANDWICH PAPER', 'We offer our customers a wide range of Paper Products such as corrugated rolls, carton, packing boxes, paper rolls, duplex boxes and many more. These Paper Products are available in various sizes, designs and variants', 'PAPER PRODUCTS', '9.5-10KG', 'TRANSPARENT', 'PS', '', '', '', '', 'images/product/826e2d2cbd581ea3bdc3398c448f1655.jpg', 'M.G. أبيض، ساندويتش، بابر', 'نحن نقدم لعملائنا مجموعة واسعة من المنتجات الورقية مثل لفات المموج، الكرتون، صناديق التعبئة، لفات الورق، صناديق مزدوجة وغيرها الكثير. هذه المنتجات الورقية متوفرة في مختلف الأحجام والتصاميم والمتغيرات', 'منتجات ورقية', '9.5-10KG', 'شفاف', 'PS'),
(30, 'FACIAL TISSUE', 'Facial tissue and paper handkerchief refers to a class of soft, absorbent, disposable papers that are suitable for use on the face. They are disposable alternatives', 'FACIAL TISSUE 200 PULLS', '5/INNER, 6 INNER/CARTON', 'WHITE1111', 'VIRGIN PULP PAPER (200/190MM)1111', 'cxxxxxxxxxxxxxxxxxx', 'dfdf', '', '', 'images/product/42c8671c40df0b4d7f96090f92bb3082.jpg', 'أنسجة الوجه', 'مناديل الوجه والورق المنديل يشير إلى فئة من أوراق لينة، ماصة، يمكن التخلص منها التي هي مناسبة للاستخدام على الوجه. وهي بدائل يمكن التخلص منها', 'فاسيال تيسو 200 بولس', '5 / الداخلية، 6 الداخلية / الكرتون', 'أبيض111111111111', 'فرجين بولب بابر (200 / 190MM)');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pro_cat_mapping`
--

CREATE TABLE `tbl_pro_cat_mapping` (
  `p_mapID` int(11) NOT NULL,
  `pcat_id` int(20) NOT NULL,
  `product_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pro_cat_mapping`
--

INSERT INTO `tbl_pro_cat_mapping` (`p_mapID`, `pcat_id`, `product_id`) VALUES
(80, 15, 27),
(102, 15, 30),
(103, 11, 30),
(104, 11, 29);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`pcat_id`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pro_cat_mapping`
--
ALTER TABLE `tbl_pro_cat_mapping`
  ADD PRIMARY KEY (`p_mapID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `pcat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `tbl_pro_cat_mapping`
--
ALTER TABLE `tbl_pro_cat_mapping`
  MODIFY `p_mapID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
